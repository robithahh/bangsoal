/*
 * The MIT License
 *
 * Copyright 2015 radityopw.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package bangsoal.dao;

import bangsoal.dao.specs.QuestionDao;
import bangsoal.entity.Question;
import bangsoal.shared.Common;
import static bangsoal.shared.Common.fileToObject;
import static bangsoal.shared.Common.objectToFile;
import bangsoal.shared.Configuration;
import static bangsoal.shared.Configuration.getInstance;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;

/**
 *
 * @author radityopw
 */
public class QuestionDaoFile implements QuestionDao {
    private static final Logger LOG = getLogger(QuestionDaoFile.class.getName());
    
    long conceptId;
    private final Map<String, Question> list;
    
    private final Configuration conf = getInstance();
    
    /**
     *
     * @param c
     */
    public QuestionDaoFile(long c){
        conceptId = c;
        list = new TreeMap<>();
    }

    /**
     *
     * @param a
     */
    @Override
    public void save(Question a) {
        File dir = new File(conf.getWorkingDir(), "questions");
        File dir2 = new File(dir, conceptId + "");
        if (!dir2.exists()) {
            dir2.mkdir();
        } 
        File file = new File(dir2, a.getId() + ".data");
        try {
            objectToFile(file.getAbsolutePath(), a);
            list.put(a.getId() + "", a);
        } catch (IOException ex) {
            getLogger(SubjectDaoFile.class.getName()).log(SEVERE, null, ex);
        }
    }

    /**
     *
     * @param a
     */
    @Override
    public void delete(Question a) {
        File dir = new File(conf.getWorkingDir(), "questions");
        File dir2 = new File(dir, "" + conceptId);
        File file = new File(dir2, a.getId() + ".data");
        if (file.exists()) {
            file.delete();
            list.remove(a.getId() + "");
            a = null;
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Question get(int id) {
        Question c = null;

        if (list == null) {
            getAll();
        }

        Iterator<Question> iterator = list.values().iterator();
        int index = -1;

        while (index < id) {
            c = iterator.next();
            index++;
        }

        return c;
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<Question> getAll() {
        if (list.isEmpty()) {
            File dir = new File(conf.getWorkingDir(), "questions");
            File dir2 = new File(dir, "" + conceptId);

            if (!dir2.exists()) {
                dir2.mkdir();
            }
            List<String> subDir = asList(dir2.list());

            subDir.stream().map((d) -> new File(dir2, d)).map((ds) -> (Question) fileToObject(ds.getAbsolutePath())).forEach((c) -> {
                list.put(c.getId() + "", c);
            });
        }
        return list.values();
    }
    
}
